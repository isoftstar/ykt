package com.huohuzhihui.api.service;

/**
 * 订单Service接口
 * 
 * @author huohuzhihui
 * @date 2020-11-15
 */
public interface ApiMerchantService
{
    /**
     * 设备号登陆认证
     * @param deviceNo
     * @return
     */
    String loginByDevice(String deviceNo);
}
