package com.huohuzhihui.framework.interceptor;

import com.alibaba.fastjson.JSON;
import com.appleyk.core.model.LicenseVerifyManager;
import com.appleyk.verify.config.LicenseVerifyProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * LicenseCheckInterceptor
 */
@Component
public class LicenseCheckInterceptor extends HandlerInterceptorAdapter{
    private static Logger logger = LogManager.getLogger(LicenseCheckInterceptor.class);
    @Autowired
    private LicenseVerifyProperties properties;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
       /* LicenseVerifyManager licenseVerifyManager = new LicenseVerifyManager();

        //校验证书是否有效
        boolean verifyResult = licenseVerifyManager.verify(this.properties.getVerifyParam()).getResult().booleanValue();

        if(verifyResult){
            return true;
        }else{
            response.setCharacterEncoding("utf-8");
            Map<String,String> result = new HashMap<>(1);
            result.put("result","您的证书无效，请核查服务器是否取得授权或重新申请证书！");

            response.getWriter().write(JSON.toJSONString(result));

            return false;
        }*/


        return true;
    }
}